#!/bin/bash

IFS=$'\n'
file=/etc/bash.bashrc

for i in `cat aliases`
do
    if ! grep "^$i$" $file > /dev/null
    then
        echo "$i" >> $file
    fi
done
