#!/bin/bash

file=/etc/bash.bashrc

if ! grep "^alias cp='cp -i'$" $file > /dev/null
then
    echo "alias cp='cp -i'" >> $file
fi

if ! grep "^alias grep='grep --color=auto'$" $file > /dev/null
then
    echo "alias grep='grep --color=auto'" >> $file
fi

if ! grep "^alias ls='ls -l --color=auto'$" $file > /dev/null
then
    echo "alias ls='ls -l --color=auto'" >> $file
fi

if ! grep "^alias mv='mv -i'$" $file > /dev/null
then
    echo "alias mv='mv -i'" >> $file
fi

if ! grep "^alias rm='rm -i'$" $file > /dev/null
then
    echo "alias rm='rm -i'" >> $file
fi

if ! grep "^alias nano='nano --tabstospaces --tabsize=4'$" $file > /dev/null
then
    echo "alias nano='nano --tabstospaces --tabsize=4'" >> $file
fi

if ! grep "^alias crontab='crontab -i'$" $file > /dev/null
then
    echo "alias crontab='crontab -i'" >> $file
fi
