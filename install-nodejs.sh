#!/bin/bash

version=$1

if [ -z "$version" ]
then
    echo 'Usage: ./install-nodejs.sh <version>'
    echo '  <version>  - Node.js version number (e.g. 0.1.2)'
    exit 1
fi

name=node-v$version
tarname=$name.tar.gz
link=http://nodejs.org/dist/v$version/$tarname

apt-get install -y build-essential libssl-dev python
cd /tmp
wget -nc $link
tar xf $tarname
cd $name
./configure
make install
node -v